//S = piRr
//P = pi (a + b) (R = a, r = b)
public class Ellipse extends Circle{
    protected int r2;

    @Override
    public double square() {
        double S = Math.PI * R1 * r2;
        System.out.println("Square = " + S);
        return S;
    }
    @Override
    public double perimeter() {
        double P = Math.PI * (R1 + r2);
        System.out.println("Perimeter =" + P);
        return P;
    }

    @Override
    public void changeSize(double coefficient) {
        this.R1 *= coefficient;
        this.r2 *= coefficient;
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }
}


