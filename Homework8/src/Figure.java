//Круг - circle
//Прямоугольник - Rectangle
//Квадрат - Square
public abstract class Figure implements Relocatable, Scalable {
protected double CoordinateX;
protected double CoordinateY;


    public abstract double square();

    public abstract double perimeter();

    @Override
   public void changeSize(double coefficient) {}

    @Override
    public void changeCoordinates(double CoordinateX, double CoordinateY) {}
    public double getCoordinateX() {
        return CoordinateX;
    }

    public void setCoordinateX(double coordinateX) {
        CoordinateX = coordinateX;
    }

    public double getCoordinateY() {
        return CoordinateY;
    }

    public void setCoordinateY(double coordinateY) {
        CoordinateY = coordinateY;
    }


}
