public interface Scalable {
    public void changeSize(double coefficient);
}
