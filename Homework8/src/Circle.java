//S = pi R^2
//P = 2piR

public class Circle extends Figure{
    protected int R1;


    @Override
    public double square() {
        double S = Math.PI * (R1 * R1);
        System.out.println("Square = " + S);
        return S;
    }
    @Override
    public double perimeter() {
        double P = 2 * Math.PI * R1;
        System.out.println("Perimeter =" + P);
        return P;
    }

    @Override
    public void changeSize(double coefficient) {
        this.R1 *= coefficient;
    }

    public int getR1() {
        return R1;
    }

    public void setR1(int r1) {
        R1 = r1;
    }
}


