//S=a^2
//P=4a
public class Square extends Figure{
    protected int A;


    @Override
    public double square() {
        double S = A * A;
        System.out.println("Square = " + S);
        return S;
    }

    @Override
    public double perimeter() {
        double P = 4 * A;
        System.out.println("Perimeter =" + P);
        return P;
    }

    @Override
    public void changeSize(double coefficient) {
        this.A *= coefficient;
    }

    public int getA() {
        return A;
    }

    public void setA(int a) {
        A = a;
    }
}
