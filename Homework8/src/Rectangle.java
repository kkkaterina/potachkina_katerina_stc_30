//S=a*b
//P=2(a+b)
public class Rectangle extends Square {
    protected int B;

    @Override
    public double square() {
        double S = A + B;
        System.out.println("Square = " + S);
        return S;
    }

    @Override
    public double perimeter() {
        double P = 2 * (A + B);
        System.out.println("Perimeter =" + P);
        return P;
    }

    @Override
    public void changeSize(double coefficient) {
        this.A *= coefficient;
        this.B *= coefficient;
    }


    public int getB() {
        return B;
    }

    public void setB(int b) {
        B = b;
    }
}

