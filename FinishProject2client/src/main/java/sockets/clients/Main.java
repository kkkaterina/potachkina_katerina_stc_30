package sockets.clients;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SocketClient client = new SocketClient("localhost", 7777);

        Scanner scanner = new Scanner(System.in);
        String message = scanner.nextLine();
        // все время читаем сообщения с клавиатуры и отправляем их на сервер
        while (!message.equals("EXIT")) {
            client.sendMessage(message);
            message = scanner.nextLine();
        }
    }

}

