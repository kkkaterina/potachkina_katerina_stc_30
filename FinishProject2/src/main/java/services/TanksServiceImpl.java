package services;
import models.Game;
import models.Player;
import models.Shot;
import repositories.*;

import java.time.LocalDate;

public class TanksServiceImpl implements TanksService {
    private ShotsRepository shotsRepository = new ShotsRepositoryImpl(null);
    private PlayersRepository playersRepository = new PlayersRepositoryImpl(null);
    private GamesRepository gamesRepository = new GamesRepositoryImpl(null);

    public Long startGame(String firstPlayerNickname, String secondPlayerNickname) {
        Player firstPlayer = playersRepository.findByNickname(firstPlayerNickname);
        Player secondPlayer = playersRepository.findByNickname(secondPlayerNickname);
              if (firstPlayer == null) {
            firstPlayer = new Player(firstPlayerNickname);
            playersRepository.save(firstPlayer);
        } else {

        }

        if (secondPlayer == null) {
            secondPlayer = new Player(secondPlayerNickname);
            playersRepository.save(secondPlayer);
        }else {

        }

        Game game = new Game();
        game.setFirst(firstPlayer);
        game.setSecond(secondPlayer);
        game.setGameDate(LocalDate.now().toString());

        gamesRepository.save(game);
        return game.getId();



    }

    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(shooter, target, game);
        if (game.getFirst().equals(shooter)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecond().equals(shooter)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        shotsRepository.save(shot);
        gamesRepository.update(game);
        System.out.println(" shots: " +
                game.getFirstPlayerShotsCount() + "|" + game.getSecondPlayerShotsCount());
    }

}
