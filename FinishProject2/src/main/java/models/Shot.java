package models;

public class Shot {
    private Player shooter;
    private Game game;

    public Shot(Player shooter, Player target, Game game) {
        this.shooter = shooter;
        this.game = game;

    }

    public Player getShooter() {
        return shooter;
    }

    public Game getGame() {
        return game;
    }

    public void setShooter(Player shooter) {
        this.shooter = shooter;
    }
    public void setGame(Game game) {
        this.game = game;
    }

}
