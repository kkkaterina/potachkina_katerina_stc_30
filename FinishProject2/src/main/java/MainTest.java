import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import repositories.*;

public class MainTest {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/Innopolis");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("Pem123456");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);


        GamesRepository gamesRepository = new GamesRepositoryImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);


        System.out.println(playersRepository.findById(1L));

    }
}
