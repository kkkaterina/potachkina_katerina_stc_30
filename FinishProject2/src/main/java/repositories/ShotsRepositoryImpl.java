package repositories;

import models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryImpl implements ShotsRepository{

    private static final String SQL_INSERT = "insert into account () values (?, ?, ?, ?);";
    private DataSource dataSource;
    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection(); //подключение к бд
             //запров в бд
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            //результат запроса
            statement.setObject(1, shot.getShooter());
            statement.setObject(2,shot.getGame());

            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                shot.setShooter((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }



            generatedIds.close();


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

}
