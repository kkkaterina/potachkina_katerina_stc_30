package repositories;


import models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class PlayersRepositoryImpl implements PlayersRepository {
    private static final String SQL_FIND_BY_ID = "select * from player where id = ?";
    private static final String SQL_INSERT = "insert into account (nickname) values (?);";
    private static final String SQL_UPDATE = "update player set " +
            "nickname = ?, ";



    public Optional<Player> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, id.intValue());
            ResultSet result = statement.executeQuery();
            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
    private DataSource dataSource;

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Player firstPlayer) {
        try (Connection connection = dataSource.getConnection(); //подключение к бд
             //запров в бд
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            //результат запроса
            statement.setString(1, firstPlayer.getNickName());
            statement.setLong(2,firstPlayer.getId());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                firstPlayer.setId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Player firstPlayer) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, firstPlayer.getNickName());
            statement.setLong(2,firstPlayer.getId());
            
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }


    @Override
    public Player findByNickname(String firstPlayerNickname) {
        return null;
    }
}