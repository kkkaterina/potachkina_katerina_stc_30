package repositories;

import models.Game;

import javax.sql.DataSource;
import java.sql.*;

public class GamesRepositoryImpl implements GamesRepository {
    private static final String SQL_INSERT = "insert into game (game_date, player_first, player_second," +
            "first_player_shots_count, second_player_shots_count, game_time) values (?, ?, ?, ?, ?, ?);";
    private DataSource dataSource;
    public GamesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection(); //подключение к бд
             //запров в бд
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            //результат запроса
            statement.setLong(1, game.getId());
            statement.setString(2,game.getGameDate());
            statement.setObject(3,game.getFirst());
            statement.setObject(4, game.getSecond());
            statement.setLong(5,game.getGameTime());
            statement.setInt(6, game.getFirstPlayerShotsCount());
            statement.setInt(7,game.getSecondPlayerShotsCount());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                game.setId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void update(Game game) {

    }

    @Override
    public Game findById(Long gameId) {
        return null;
    }

}
