package repositories;

import models.Game;

public interface GamesRepository {
    void save(Game game);

    void update(Game game);

    Game findById(Long gameId);

}
