package repositories;

import models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
