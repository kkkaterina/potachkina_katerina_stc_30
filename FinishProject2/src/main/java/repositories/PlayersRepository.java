package repositories;

import models.Player;

import java.util.Optional;

public interface PlayersRepository {
    Optional<Player> findById(Long id);

    void save(Player firstPlayer);
    void update (Player firstPlayer);

    Player findByNickname(String firstPlayerNickname);
}
