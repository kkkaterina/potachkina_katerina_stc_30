public class Program2 {
    public static double f(double x) {
        return Math.sin(x);
    }

    public static double integralByRectangles(double a, double b, int n) {
        double h = (b - a) / n;

        double sum = 0;

        for (double i = a + h; i < b; i += h * 2) {
            sum += f(i - h) + 4 * f(i) + f(i + h);

            return sum;
        }
    }
    public static void printIntegralResultsForN(double a, double b, int [] ns) {
        for (int i = 0; i < ns.length; i++) {
            System.out.println("For N = " + ns[i] + ", result = " + integralByRectangles(a, b, ns[i]));
        }
    }


    public static void main(String[] args) {
        int ns[] = {10, 100, 1000, 10000 };

        printIntegralResultsForN(0, 10, ns);
    }
}
