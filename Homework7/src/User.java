public class User {

 private String firstName;
 private String lastName;
 private int age;
 private boolean isWorker;
 public static class Builder {
  public User newUser;
  //idea предложила сделать пакет приватным
  Builder(){
   newUser = new User();
  }
  public Builder setFirstName(String firstName){
   newUser.firstName = firstName;
   System.out.println(firstName);
   return this;
  }
  public Builder setLastName (String lastName){
   newUser.lastName = lastName;
   System.out.println(lastName);
   return this;
  }
  public Builder setAge(int age){
   newUser.age = age;
   System.out.println(age);
   return this;
  }
  public Builder setWorker(boolean isWorker){
   newUser.isWorker = isWorker;
   System.out.println(isWorker);
   return this;
  }
  public User build (){
   return newUser;
  }
 }

 public String getFirstName() {
  return firstName;
 }

 public String getLastName() {
  return lastName;
 }

 public int getAge() {
  return age;
 }

 public boolean isWorker() {
  return isWorker;
 }
}
