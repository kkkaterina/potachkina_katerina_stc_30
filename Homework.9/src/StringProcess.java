public interface StringProcess {
    String process(String word);
}