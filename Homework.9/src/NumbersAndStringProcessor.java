public class NumbersAndStringProcessor {
    private int [] numberArrays;
    private String [] stringsArrays;
    private int count = 0;

    public NumbersAndStringProcessor(int[] numberArrays, String[] stringsArrays) {
        this.numberArrays = numberArrays;
        this.stringsArrays = stringsArrays;
    }
    public int[] processor(NumbersProcess process) {
        int [] result = new int[numberArrays.length];
        for (int i = 0; i < numberArrays.length; i++) {
            result[i] = process.process(numberArrays[i]);
        }
        return result;
    }
    public String [] processor (StringProcess process){
        String [] result = new String[stringsArrays.length];
        for (int i = 0; i < stringsArrays.length; i++) {
            result[i] = process.process(stringsArrays[i]);
        }
        return result;
    }

}
