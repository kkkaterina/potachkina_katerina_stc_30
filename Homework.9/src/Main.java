import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] numbersArrays = {1, 3, 5040, 5, 6, 102, 304, 1028, 60540};
        String[] stringsArray = {"Last", "Ch1ristmas", "I5", "g1ave", "y2ou", "m4y", "hear2t"};
        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(numbersArrays, stringsArray);

        NumbersProcess reversNumber = new NumbersProcess() {
            @Override
            public int process(int number) {
                int reverse = 0;
                while (number != 0) {
                    reverse *= 10;
                    reverse = reverse + number % 10;
                    number /= 10;
                }
                return reverse;
            }
        };

        NumbersProcess deleteZero = new NumbersProcess() {
            @Override
            public int process(int number) {
                StringBuilder result = new StringBuilder();
                while (number != 0) {
                    if (number % 10 != 0) {
                        result.append(number %10);
                    }
                    number /= 10;
                }
                return Integer.parseInt(result.reverse().toString());
            }
        };

        NumbersProcess changeDigit = new NumbersProcess() {
            @Override
            public int process(int number) {
                StringBuilder result = new StringBuilder();
                while (number != 0) {
                    int digit = number %10;

                    if(digit%2==0)

                        ++digit;
                    result.append(digit);

                    number /= 10;
                }
                return Integer.parseInt(result.reverse().toString());
            };
        };


        StringProcess reversString = new StringProcess() {
            @Override
            public String process(String process) {
                return new StringBuilder(process).reverse().toString();
            }
        };
        StringProcess deleteNumbers = new StringProcess() {
            @Override
            public String process(String word) {
                word = word.replaceAll("[0-9]","");
                return word;
            }
        };

        System.out.println(Arrays.asList(numbersAndStringProcessor.processor(reversNumber)));
        for (int i : numbersAndStringProcessor.processor(deleteZero)) {
            System.out.print(i + " " );
        }
        System.out.println();

        for (String i: numbersAndStringProcessor.processor(deleteNumbers)){
                System.out.print(i + " ");
        }
        System.out.println();


     }
}
