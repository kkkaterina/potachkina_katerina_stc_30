public class LinkedList<E> implements List<E> {

    private Node next;

    private class LinkedListIterator implements Iterator<E> {

        @Override
        public E next() {
            return null;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node<E> first;
    private Node<E> last;
    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }

        public void setNext(Node<F> next) {
            this.next = next;
        }

    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;


    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

        @Override
    public void removeByIndex(int index) {
        int i = 0;
       Node<E> current = this.first;
        while (current != null) {
            if ((i + 1) == index) {
                current.setNext(current.next.next);
                count--;
                return;
            } else {
                current = current.next;
                i++;
            }
        }
    }
    @Override
    public void insert(E element, int index) {
        Node<E> current = this.first;
        Node<E> current1 = current.next;
       Node<E> newNode = new Node<E>(element);
        if (index <= count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = newNode;
            newNode.next = current1;
            count++;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            count++;
        }

    }

    @Override
    public void reverse() {
        Node<E> first = null;
        Node<E> current = null;

        Node<E> last = null;

        while (current != null) {

            next = current.next;

            current.next = first;

            first = current;

            current = next;
        }
    }

    @Override
    public void removeFirst(E element, int index) {
        int i = 0;
        Node<E> current = this.first;
        while (current != null) {
            if ((i + 1) == index) {
                current.setNext(current.next.next);
                count--;
                return;
            } else {
                current = current.next;
                i++;
            }
        }

    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<E>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }


    @Override
    public boolean contains(E element) {
        return false;
    }

    @Override
    public int size() {
        return count;
    }


    // реализация метода получения объекта
    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }
}
