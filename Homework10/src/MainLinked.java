public class MainLinked {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();

        list.add(14);
        list.add(88);
        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        list.reverse();
    }
}
