import java.util.Scanner;

public class RemoteController {

    public void inputButton(TV tv1) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int button = scanner.nextInt();
            if (button > 3 || button < 1) {
                System.out.println("Please, push the other button. From 1 to 3");
            } else if (button == 1 || button == 2 || button == 3) {
                tv1.inputSignal(button);
            }
        }
    }
}


