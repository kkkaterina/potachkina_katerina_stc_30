import java.util.Scanner;
import java.util.Arrays;
public class Program2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //консольный ввод массива
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        Arrays.sort(array);
        System.out.println(Arrays.toString(array)); //вывод отсортированного массива
        System.out.println("Enter number for search: "); //число для бинарного поиска
        int numberForSearch = scanner.nextInt();
        System.out.println(binarySearch(array, numberForSearch));


    }
    public static int[] binarySearch(int [] array, int numberForSearch, int firstNumber, int lastNumber){
        int middleNumber = (firstNumber + lastNumber) / 2;
        if (array [middleNumber] == numberForSearch) {

            if (array [middleNumber] > numberForSearch) {
                return binarySearch(array, numberForSearch, firstNumber, middleNumber - 1);

            } else {
                return binarySearch(array, numberForSearch, firstNumber, middleNumber);
            }
        }
        return middleNumber;
    }

}







